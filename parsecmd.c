#include "parsecmd.h"

void putData(Vector * vec,char * tok)
{
	strncpy(vec->tokens[vec->size],tok,79);
	vec->size++;
}

Vector * parsecmd(char *str,char *delim)
{
	Vector *v = malloc(sizeof(Vector));
	v->size = 0;
	v->inredirInd = -1;
	v->outredirInd = -1;
	v->pipeInd = -1;
	char * saveptr;
	char * tok = strtok_r(str,delim,&saveptr);
	if(tok != NULL)
		putData(v,tok);
	int index = 1;
	while((tok = strtok_r(NULL," ",&saveptr)) != NULL){
		if(strcmp(tok,"|") == 0)
			v->pipeInd = index;
		if(strcmp(tok,"<") == 0)  
			v->inredirInd = index;
		if(strcmp(tok,">") == 0)
			v->outredirInd = index;
		putData(v,tok);
		index++;
	}
	return v;
}

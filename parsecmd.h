#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct _vec {
	int size;
	char tokens[10][80];
	int pipeInd;
	int outredirInd;
	int inredirInd;
}Vector;

//puts data into Vector
void putData(Vector * vec,char * tok);

//parse command and checks whether command has redirections and pipe operator
Vector * parsecmd(char *str,char *delim);

void handle_error();

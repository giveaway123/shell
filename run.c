#include "parsecmd.h"
#include <unistd.h>
#include <sys/wait.h> 
#include <fcntl.h>

int Open_File(const char * file)
{
	int fd = open(file,O_RDWR | O_CREAT | O_TRUNC,S_IWUSR | S_IRUSR);
	if(fd < 0){
			puts("file does not exists");
			exit(1);
		}
	return fd;
}
//input redirection function
//This function opens a file,reads it and writes its output in pipe or stdout
void runsrc2(int pfd[],Vector * v)
{
	switch(fork()){
		case -1:
			puts("fork error");
			exit(1);
		case 0:;
		       int fd = open(v->tokens[v->inredirInd+1],O_RDONLY,S_IRUSR);
		       if(fd < 0){
			       puts("Unable to open file");
			       exit(1);
		        }
		  	close(pfd[0]);
			dup2(fd,0);
			if(v->pipeInd > 0){
				dup2(pfd[1],1);
			}
			else if(v->outredirInd > 0){
				int fd2 = Open_File(v->tokens[v->outredirInd+1]);
				dup2(fd2,1);
			}
			char *argv[v->inredirInd];
			for(int i = 0; i < v->inredirInd;i++)
				argv[i] = v->tokens[i];
			argv[v->inredirInd] = NULL;
			execvp(argv[0],argv);
			exit(1);
	}
			
}
void runsrc(int pfd[],Vector * v)
{
	int rc = fork();
	if (rc < 0)
	{
		puts("fork failed");
		exit(1);
	}
	else if (rc == 0)
	{
		//child process
		Vector temp = *v;
		int size = temp.size;
		int fd;
		if(v->pipeInd > 0)
		{
			close(pfd[0]);
			dup2(pfd[1],1);
			size = v->pipeInd;	
		}
		else if (v->outredirInd > 0)
		{
			size = v->outredirInd;
			fd = Open_File(v->tokens[v->outredirInd+1]);
			dup2(fd,1);
		}
		char *args[size];
		for(int i = 0; i < size;i++){
			args[i] = temp.tokens[i];
		}
		args[size] = NULL;
		execvp(args[0],args);	
		puts("Unable to run command. Please try again!");	
	}
}
void rundest(int pfd[],Vector *v)
{
	int rc = fork();
	if (rc < 0)
	{
		puts("fork failed");
		exit(1);
	}
	if(rc == 0 )
	{
		dup2(pfd[0],STDIN_FILENO);
		close(pfd[1]);
		close(pfd[0]);
		int fd;
		char *argv[v->size-v->pipeInd];
		int size = v->size;
		if(v->outredirInd > 0){
			size = v->outredirInd;
			fd = Open_File(v->tokens[v->outredirInd+1]);
			dup2(fd,1);
		}
		int ind = 0;
		for(int i = v->pipeInd+1;i < size;i++){
			argv[ind] = v->tokens[i];
			ind++;
		}
		argv[ind] = NULL;
		execvp(argv[0],argv);
		puts("fail");
	}
	
}
void runcmd(Vector * v)
{
	int pfd[2];
	int ret = pipe(pfd);
	if (ret < 0)
	{
		puts("pipe err");
		exit(1);
	}
	if(v->inredirInd > 0 && v->pipeInd > 0){
		runsrc2(pfd,v);
		rundest(pfd,v);
	}
	else if(v->pipeInd > 0){
		runsrc(pfd,v);
		rundest(pfd,v);
	}
	else if((v->inredirInd > 0 && v->outredirInd > 0) | (v->inredirInd > 0))
		runsrc2(pfd, v);
	else if(v->outredirInd > 0)
		runsrc(pfd,v);
	else
		runsrc(pfd,v);
	close(pfd[0]);
	close(pfd[1]);
	wait(0);
	wait(0);
}

int main()
{
	char buffer[128];
	unsigned int num = 1;
	while(1){
		printf("mysh(%u)> ",num);
		fgets(buffer,127,stdin);
		buffer[strlen(buffer)-1] = 0;
		if(strcmp("exit",buffer) == 0)
			exit(1);
		Vector * v = parsecmd(buffer," ");
		runcmd(v);
		num++;
		free(v);
	}
	return 0;
}

Simplified version of linux command line interface also know as shell.
Supports loading of built-in linux commands such as grep,cat
Supports input and output redirection and pipe operator.

Runs on any unix like systems and WSL.

Compile: run the command gcc -o mysh parsecmd.c run.c in your shell
then run ./mysh